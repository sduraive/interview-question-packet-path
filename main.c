#include <stdio.h>
#include <time.h>
#define CHUNK_SIZE 5000

// Helper Function
// Finds the position of two strings  where they differ
// Returns the position of the string or -1 if they are the same
int compareBytes(const char *x, const char *y, int size){
    int i = 0;
    while(i < size){
        if(x[i] != y[i]){
            return i;
        }
        i++;
    }
    return -1;
}

// Helper Function
// Prints 16 bytes at the desired offset
FILE* PrintBytes(FILE *fp, char* fileName,int chunkPosition, char *data ){
    int bytesPrinted = 0;
    int filePos = chunkPosition;
    printf("%s ", fileName);
    while(filePos < CHUNK_SIZE+1 && bytesPrinted < 16){
        printf("%hhx ", data[filePos]);
        filePos++;
        bytesPrinted++;
    }
    if(bytesPrinted < 16){
        size_t elements_read = fread_s(&data,CHUNK_SIZE,CHUNK_SIZE,1, fp );
        if(elements_read == 0){
            return 0;
        }
        int i = 0;
        while(i < CHUNK_SIZE+1 && bytesPrinted < 16){
            printf("%hhx ", data[i]);
            i++;
            bytesPrinted++;
        }
    }
    return fp;
}

int main(int argc, char *argv[])
{
    // start clock time
    clock_t t;
    t = clock();

    // checks if you gave the specified arguments
    if (argc != 3){
        printf("Too few or too many arguements\n");
        return -1;
    }

    // opens both files in reading binary mode
    FILE* fileP1;
    FILE *fileP2;

    fopen_s(&fileP1, argv[1], "rb");
    if(fileP1 == NULL){
        printf("Can not open %s\n", argv[1]);
        return -1;
    }

    fopen_s(&fileP2, argv[2], "rb");
    if(fileP2 == NULL){
        printf("Can not open %s\n", argv[2]);
        return -1;
    }

   //buffer arrays to store values from the file
    char num1[CHUNK_SIZE+1] = "";
    char num2[CHUNK_SIZE+1] = "";

    // finds the specific file size
    fseek( fileP1, 0, SEEK_END);
    long int fileSize = ftell(fileP1);
    fseek( fileP1, 0, SEEK_SET);


    num1[CHUNK_SIZE] = '\0';
    num2[CHUNK_SIZE] = '\0';

    int numChunks = 1;
    int chunkPosition = -1;

    int readSize = 0;
    // loop to check values are the same in both file
    // breaks when found first difference
    while(fileSize > 0){
        if(fileSize - CHUNK_SIZE > 0){
            fileSize -= CHUNK_SIZE;
            readSize = CHUNK_SIZE;
        }else{
            readSize = fileSize;
            fileSize  = 0;
        }
        fread_s(&num1, CHUNK_SIZE,readSize,1, fileP1);
        fread_s(&num2, CHUNK_SIZE,readSize,1, fileP2 );
        chunkPosition = compareBytes(num1, num2, readSize);

        if(chunkPosition == -1) {
            numChunks++;
        }else{
            break;
        }
        num1[readSize] = '\0';
        num2[readSize] = '\0';
    }

    // if both files are the same
    if(chunkPosition == -1){
        printf("Both Files Are Identical");
        t = clock() - t;
        double time_taken = ((double)t);
        printf("Program took %f milliseconds to execute \n", time_taken);
        return 1;
    }
    // if files have some difference
    else{
        printf("Files are different starting at position: %d\n\n", (numChunks*chunkPosition)+1);
        fileP1 = PrintBytes(fileP1, argv[1],chunkPosition, num1);
        printf("\n\n");
        fileP2 = PrintBytes(fileP2, argv[2],chunkPosition, num2);
    }
    //close files
    fclose(fileP1);
    fclose(fileP2);

    // calculate total time
    t = clock() - t;
    double time_taken = ((double)t);
    printf("\n \nProgram took %f milliseconds to execute \n", time_taken);
    return 1;
}
